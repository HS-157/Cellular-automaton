from tkinter import *
import pprint
import random

size = 1000
square = 10

class Cell():
    def __init__(self, size, x, y, pos, state=0, color1='black', color2='white'):
        self.size = size
        self.state = state
        self.color1 = color1
        self.color2 = color2
        self.x1 = y
        self.y1 = x
        self.x2 = y + self.size
        self.y2 = x + self.size
        self.pos = pos

    def toggle(self):
        if self.state:
            self.state = 0
        else:
            self.state = 1

    def draw(self, window):
        if self.state:
            color = self.color1
        else:
            color = self.color2

        window.create_rectangle(self.x1, self.y1, self.x2, self.y2, fill=color, width=1)

    def set_pos(self, x, y):
        self.x1 = x
        self.y1 = y
        self.x2 = x + self.size
        self.y2 = y + self.size


class Canvas_conway():
    def __init__(self, size, square):
        self.tk = Tk()
        self.w = Canvas(self.tk, width=size, height=size, background='white')
        self.w.pack()
        self.table = []
        self.size = size
        self.square = square
        self.create_table()

    def update(self):
        self.set_rules()
        self.draw_cell()
        self.tk.after(1000, self.update)

    def canvas(self):
        self.draw_cell()
        self.tk.after(1000, self.update)
        self.tk.mainloop()

    def draw_cell(self):
        for i in self.table:
            for j in i:
                j.draw(self.w)


    def create_table(self):
        x_pos = 0

        for x in range(0, self.size, self.square):
            y_pos = 0
            self.table.append([])
            for y in range(0, self.size, self.square):
                cell_pos = (x_pos, y_pos)
                self.table[x_pos].append(Cell(self.square, x, y, cell_pos))
                y_pos += 1
            x_pos += 1

    def set_rules(self):
        tmp_table = [[None for i in range(len(self.table))] for j in range(len(self.table))]
        
        for ligne in self.table:
            for cell in ligne:
                x = cell.pos[0]
                y = cell.pos[1]
                state = cell.state

                state_cells = []
                pos_cells = [
                    (x-1, y-1),
                    (x-1, y),
                    (x-1, y+1),
                    (x, y-1),
                    (x, y+1),
                    (x+1, y-1),
                    (x+1, y),
                    (x+1, y+1)
                ]

                for pos in pos_cells:
                    if pos[0] < 0:
                        state_cells.append(0)
                    elif pos[1] < 0:
                        state_cells.append(0)
                    elif pos[0] > len(self.table) - 1:
                        state_cells.append(0)
                    elif pos[1] > len(self.table) - 1:
                        state_cells.append(0)
                    else:
                        state_cells.append(self.table[pos[0]][pos[1]].state)

                if cell.state == 0:
                    if state_cells.count(1) == 3:
                        tmp_table[x][y] = 1
                    else:
                        tmp_table[x][y] = 0
                else:
                    if state_cells.count(1) in [2, 3]:
                        tmp_table[x][y] = 1
                    elif state_cells.count(1) in [0, 1, 4, 5, 6, 7, 8]:
                        tmp_table[x][y] = 0
                    else:
                        print('Error state cell : (%s, %s)' % (x, y))
                        print('State cells : %s ' % state_cells)
                        exit(1)

        len_table = len(self.table)
        for i in range(len_table):
            for j in range(len_table):
                self.table[i][j].state = tmp_table[i][j]

c = Canvas_conway(size, square)

c.table[20][50].state = 1
c.table[20][51].state = 1
c.table[20][52].state = 1

for i in range(2000):
    cell_r = random.choice(random.choice(c.table))
    cell_r.toggle()

c.canvas()
