from tkinter import *

size = 1000
square = 5

rules = [0, 1, 1, 1, 1, 1, 1, 0]

class Box():
    def __init__(self, size, x, y, state=0, color1='black', color2='white'):
        self.size = size
        self.state = state
        self.color1 = color1
        self.color2 = color2
        self.x1 = y
        self.y1 = x
        self.x2 = y + self.size
        self.y2 = x + self.size

    def toggle(self):
        if self.state:
            self.state = 0
        else:
            self.state = 1

    def draw(self, window):
        if self.state:
            color = self.color1
        else:
            color = self.color2

        window.create_rectangle(self.x1, self.y1, self.x2, self.y2, fill=color, width=1)

    def set_pos(self, x, y):
        self.x1 = x
        self.y1 = y
        self.x2 = x + self.size
        self.y2 = y + self.size


class Cell():
    def __init__(self, size, square, init, rules):
        self.master = Tk()
        self.w = Canvas(self.master, width=size, height=size, background='white')
        self.w.pack()
        self.state = []
        self.neighbour = [[1, 1, 1], [1, 1, 0], [1, 0, 1], [1, 0, 0], [0, 1, 1], [0, 1, 0], [0, 0, 1], [0, 0, 0]]
        self.size = size
        self.square = square
        self.init = init
        self.rules = rules
        self.init_state()
        self.next_ligne()

    def canvas(self):
        mainloop()

    def create_state(self, ligne):
        for i in self.state:
            i.set_pos(i.x1, ligne * self.square)
            i.draw(self.w)

    def init_state(self):
        for i in range(0, self.size, square):
            b = Box(self.square, 0, i)
            self.state.append(b)

        center = int(len(self.state) / 2)
        begin = center - int(len(self.init) / 2)
        j = 0
        for i in range(begin, begin + len(self.init)):
            self.state[i].state = self.init[j]
            j += 1

    def set_rules(self):
        state_box = []
        for i in range(len(self.state)):
            if i == 0:
                neighbour = [0, self.state[i].state, self.state[i + 1].state]
            elif i == len(self.state) - 1:
                neighbour = [self.state[i - 1].state, self.state[i].state, 0]
            else:
                neighbour = [self.state[i - 1].state, self.state[i].state, self.state[i + 1].state]

            state_box.append(self.rules[self.neighbour.index(neighbour)])

        for i in range(len(self.state)):
            self.state[i].state = state_box[i]

    def next_ligne(self):
        for i in range(len(self.state)):
            self.create_state(i)
            self.set_rules()

c = Cell(size, square, [0, 1, 1, 0], rules)
c.canvas()
