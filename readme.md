# Jeu de la vie

Besoin de Python3 et TKinter.

C'est moche mais ça marche

## Comment ça marche

### cellule.py

C'est un automates cellulaires simples sur une ligne.

~~~bash
python3 cellule.py
~~~

On peut changer les règles de génération avec la variable *rules*.

### conway.py

C'est le jeu de la vie, il y a juste à lancer pour que ça vie !

~~~bash
python3 conway.py
~~~

## Capture d'écran

![Screenshot 1](screenshot1.png)

![Screenshot 2](screenshot2.png)
